////  TmpView.swift
//  CustomSideOutMenu2
//
//  Created on 09/04/2021.
//  
//

import SwiftUI

struct TmpView: View {
    
    @State private var flag: Bool = true
    @Namespace var nspace
    
    var body: some View {
        HStack {
            if flag {
                Rectangle().fill(Color.green)
                    .matchedGeometryEffect(id: "geoeffect1", in: nspace)
                    .frame(width: 100, height: 100)
            }
            
            Spacer()
            
            Button("Switch"){
                //withAnimation(.spring())
                withAnimation(.easeInOut(duration: 1.0))
                { flag.toggle() }
            }
            
            Spacer()
            
            if !flag {
                Circle().fill(Color.blue)
                    .matchedGeometryEffect(id: "geoeffect1", in: nspace)

                    .frame(width: 50, height: 50)
            }
        }
        .padding(.all,24)
        .frame(width: 400)
        
    }
}

struct TmpView_Previews: PreviewProvider {
    static var previews: some View {
        TmpView()
    }
}


//struct TmpView: View {
//
//    @State private var rememberMe = false
//
//       var body: some View {
//           VStack {
//               PushButton(title: "Remember Me", isOn: rememberMe)
//               Text(rememberMe ? "On" : "Off")
//           }
//       }
//}
//
//struct PushButton: View {
//
//    let title: String
//    @State var isOn: Bool
//
//    var onColors = [Color.red, Color.yellow]
//    var offColors = [Color(white: 0.6), Color(white: 0.4)]
//
//    var body: some View {
//
//        Button(title) {
//            self.isOn.toggle()
//        }
//        .padding()
//        .background(LinearGradient(gradient: Gradient(colors: isOn ? onColors : offColors), startPoint: .top, endPoint: .bottom))
//        .foregroundColor(.white)
//        .clipShape(Capsule())
//        //.shadow(radius: isOn ? 0 : 5)
//
//    }
//}


//struct TmpView: View {
//
//    @State private var rememberMe = false
//
//    var body: some View {
//
//        VStack {
//
//            PushButton(isOn: $rememberMe)
//
//            Button(action: {
//                rememberMe.toggle()
//            }, label: {
//                Text("\(rememberMe ? "rem true" : "rem false")")
//            })
//            Text(rememberMe ? "On" : "Off")
//        }
//    }
//}
//
//struct PushButton: View {
//
//    @Binding var isOn: Bool
//
//    var body: some View {
//
//        Button(action: {
//            isOn.toggle()
//        }, label: {
//            Text("\(isOn ? "true" : "false")")
//        })
//
//    }
//}
