////  CustomSideOutMenu2App.swift
//  CustomSideOutMenu2
//
//  Created on 08/04/2021.
//  
//

import SwiftUI

@main
struct CustomSideOutMenu2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
