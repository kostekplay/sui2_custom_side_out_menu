////  SideMenuView.swift
//  CustomSideOutMenu2
//
//  Created on 08/04/2021.
//  
//

import SwiftUI

struct SideMenuView: View {
    
    @Binding var selectTab: String
    @Namespace var animation
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 15, content: {
            Image("Image")
                .resizable()
                .scaledToFill()
                .frame(width: 75, height: 75)
                .cornerRadius(12)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(Color.white, lineWidth: 1)
                )
                .padding(.top, 50)
            
            VStack(alignment: .leading, spacing: 10, content: {
                
                Text("Izabela Kostowska")
                    .foregroundColor(.white)
                    .font(.title2)
                    .fontWeight(.heavy)
                
                Button(action: {
                }, label: {
                    Text("View Profile")
                        .font(.subheadline)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        .padding(.all,5)
                        .background(Color.gray)
                        .cornerRadius(8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                .stroke(Color.white, lineWidth: 1)
                        )
                })
            })
            
            VStack (alignment: .leading, spacing: 8, content: {
                OverlapView(image: "house", title: "Home", selTab: $selectTab, animation: animation)
                OverlapView(image: "clock.arrow.circlepath", title: "History", selTab: $selectTab, animation: animation)
                OverlapView(image: "bell.badge", title: "Notifications", selTab: $selectTab, animation: animation)
                OverlapView(image: "gearshape.fill", title: "Settings", selTab: $selectTab, animation: animation)
                OverlapView(image: "questionmark.circle", title: "Help", selTab: $selectTab, animation: animation)
            })
            .padding(.leading,-15)
            .padding(.top,50)
            
            Spacer()
            
            VStack(alignment: .leading, spacing: 6, content: {
                
                OverlapView(image: "rectangle.righthalf.inset.fill.arrow.right", title: "Log out", selTab: .constant(""), animation: animation)
                    .padding(.leading,-15)
                
                Text("App Version 1.2.34")
                    .font(.caption)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .opacity(0.6)
            })
            
        })
        .padding()
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
    }
}

struct SideMenuView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//struct SideMenuView_Previews: PreviewProvider {
//
//    struct AddAnimationView: View {
//
//        @Namespace var ns
//
//        var body: some View {
//            SideMenuView(selectTab: .constant("Home"), animation: _ns)
//        }
//    }
//
//    static var previews: some View {
//        AddAnimationView()
//    }
//}

