////  OverlapView.swift
//  CustomSideOutMenu2
//
//  Created on 08/04/2021.
//  
//

import SwiftUI

struct OverlapView: View {
    
    let image: String
    let title: String
    
    @Binding var selTab: String
    
    var animation: Namespace.ID
    
    var body: some View {
        
        Button(action: {
            //TODO: - wyjaśnij TO !!!
            //withAnimation(.spring())
            withAnimation(.easeInOut(duration: 0.30))
                {selTab = title}
        }, label: {
            HStack(alignment: .center, spacing: 12, content: {
                Image(systemName: image)
                    .font(.title2)
                    .frame(width: 30)
                Text(title)
                    .fontWeight(.semibold)
            })
            .foregroundColor(selTab == title ? Color("green") : .white)
            .padding(.vertical,12)
            .padding(.horizontal,8)
            .frame(maxWidth: getRect().width - 200, alignment: .leading)
            .background(
                ZStack{
                    if selTab == title{
                        Color.white
                            .opacity(selTab == title ? 1 : 0)
                            .clipShape(CustomCornersView(corners: [.topRight,.bottomRight], radius: 12))
                            //TODO: - wyjaśnij TO !!!
                            .matchedGeometryEffect(id: "TAB", in: animation)
                    }
                }
            )
        })
    }
}

struct CustomCornersView: Shape {
    
    var corners: UIRectCorner
    var radius: CGFloat
    
    func path(in rect: CGRect) -> Path {
        
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension View{
    
    func getRect()->CGRect{
        return UIScreen.main.bounds
    }
}

struct OverlapView_Previews: PreviewProvider {
    
    struct AddView: View {
        
        @Namespace var ns
        
        var body: some View {
            OverlapView(image: "house", title: "Home", selTab: .constant("Home"), animation: ns)
        }
    }
    
    static var previews: some View {
            AddView()
    }
}

