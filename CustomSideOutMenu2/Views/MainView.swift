////  MainView.swift
//  CustomSideOutMenu2
//
//  Created on 10/04/2021.
//  
//

import SwiftUI

struct MainView: View {
    
    @Binding var selectedTab: String
    
    init(selectedTab: Binding<String>) {
        self._selectedTab = selectedTab
        //UITabBar.appearance().isHidden = true
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().unselectedItemTintColor = UIColor.black
    }
    
    var body: some View {
        
        TabView(selection: $selectedTab,
                content:  {
                    HomeView().tabItem {Image(systemName: "house")}.tag("Home")
                    HistoryView().tabItem {Image(systemName: "clock.arrow.circlepath")}.tag("History")
                    NotificationsView().tabItem {Image(systemName: "bell.badge")}.tag("Notification")
                    SettingsView().tabItem {Image(systemName: "gearshape.fill")}.tag("Settings")
                    HelpView().tabItem {Image(systemName: "questionmark.circle")}.tag("Help")
                })
            .accentColor(Color("green"))
    }
}

struct HomeView: View {
    
    var body: some View{
        
        NavigationView{
            
            VStack(alignment: .leading,spacing: 20){
                
                Image("Image")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: getRect().width - 50, height: 400)
                    .cornerRadius(20)
                
                VStack(alignment: .leading, spacing: 5, content: {
                    
                    Text("Izabela Kostowska")
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(.primary)
                    
                    Text("iJustine's Sister, YoutTuber ,Techie....")
                        .font(.caption)
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                })
            }
            .navigationTitle("Home")
        }
    }
}

struct HistoryView: View {
    
    var body: some View{
        
        NavigationView{
            
            Text("History")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .foregroundColor(.primary)
                .navigationTitle("History")
        }
    }
}

struct NotificationsView: View {
    
    var body: some View{
        
        NavigationView{
            
            Text("Notifications")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .foregroundColor(.primary)
                .navigationTitle("Notifications")
        }
    }
}

struct SettingsView: View {
    
    var body: some View{
        
        NavigationView{
            
            Text("Settings")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .foregroundColor(.primary)
                .navigationTitle("Settings")
        }
    }
}

struct HelpView: View {
    
    var body: some View{
        
        NavigationView{
            
            Text("Help")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .foregroundColor(.primary)
                .navigationTitle("Help")
        }
    }
}


struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(selectedTab: .constant("Home"))
    }
}
